import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static List<Car> getListFromFile(String fileName) {
        List<Car> cars = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line = reader.readLine();
            while ((line != null)) {
                String[] parts = line.split("\\|");

                String number = parts[0];
                String model = parts[1];
                String color = parts[2];
                int carMileage = Integer.parseInt(parts[3]);
                int price = Integer.parseInt(parts[4]);

                Car newCar = new Car(number, model, color, carMileage, price);
                cars.add(newCar);
                line = reader.readLine();
            }
            return cars;
        }catch (IOException e) {
            throw new IllegalArgumentException(e);

        }
    }

    public static void main(String[] args) {

        System.out.println("Номера всех автомобилей, имеющих черный цвет или нулевой пробег - ");
        getListFromFile("input.txt").stream()
                .filter(car -> car.getColor().equals("Black") || car.getCarMileage() == 0)
                .forEach(car -> System.out.println(car.getNumber()));

        System.out.println("Количество уникальных моделей в ценовом диапазоне от 70 до 80 тыс. - ");
        System.out.println(
                getListFromFile("input.txt").stream()
                        .filter(car -> car.getPrice() >= 70000 && car.getPrice() <= 80000)
                        .map(car -> car.getModel())
                        .distinct()
                        .count());
    }
}
